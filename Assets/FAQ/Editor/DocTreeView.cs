﻿using System;
using System.IO;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

class DocTreeView : TreeView
{
    private string _pathKey = "DocPath";
    public string PathKey { get { return _pathKey; } }
    private int i = 1;

    public DocTreeView(TreeViewState treeViewState)
        : base(treeViewState)
    {
        Reload();
    }

    protected override TreeViewItem BuildRoot()
    {
        TreeViewItem root = new TreeViewItem { id = 0, depth = -1, displayName = Path.GetFileName(EditorPrefs.GetString(_pathKey, Application.dataPath)) };
        GetFolders(EditorPrefs.GetString(_pathKey, Application.dataPath), root);
        GetFiles(EditorPrefs.GetString(_pathKey, Application.dataPath), root);

        SetupDepthsFromParentsAndChildren(root);

        return root;
    }

    private void GetFolders(string path, TreeViewItem parent)
    {
        string[] directories = Directory.GetDirectories(path);
        if (directories.Length > 0)
        {
            foreach (string directory in directories)
            {
                try
                {
                    if ((File.GetAttributes(directory) & FileAttributes.ReparsePoint) != FileAttributes.ReparsePoint
                        && (File.GetAttributes(directory) & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        TreeViewItem item = new TreeViewItem { id = i, displayName = Path.GetFileName(directory) };
                        item.icon = EditorGUIUtility.FindTexture("Folder Icon");
                        parent.AddChild(item);
                        i++;
                        GetFolders(directory, item);
                        GetFiles(directory, item);
                    }
                } catch (UnauthorizedAccessException) { /* Error when he does not have access to the directory */ }
            }
        }
    }

    private void GetFiles(string path, TreeViewItem parent)
    {
        string[] files = Directory.GetFiles(path);
        if (files.Length > 0)
        {
            foreach (string file in files)
            {
                try
                {
                    if ((File.GetAttributes(file) & FileAttributes.ReparsePoint) != FileAttributes.ReparsePoint
                        && (File.GetAttributes(file) & FileAttributes.Hidden) != FileAttributes.Hidden
                        && Path.GetExtension(file) != ".meta")
                    {
                        TreeViewItem item = new TreeViewItem { id = i, displayName = Path.GetFileName(file) };
                        item.icon = GetIconForFile(Path.GetExtension(file));
                        parent.AddChild(item);
                        i++;
                    }
                } catch (UnauthorizedAccessException) { /* Error when he does not have access to the file */ }
                
            }
        }
    }


    private static Texture2D GetIconForFile(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".boo":
                return EditorGUIUtility.FindTexture("boo Script Icon");
            case ".cginc":
                return EditorGUIUtility.FindTexture("CGProgram Icon");
            case ".cs":
                return EditorGUIUtility.FindTexture("cs Script Icon");
            case ".guiskin":
                return EditorGUIUtility.FindTexture("GUISkin Icon");
            case ".js":
                return EditorGUIUtility.FindTexture("Js Script Icon");
            case ".mat":
                return EditorGUIUtility.FindTexture("Material Icon");
            case ".prefab":
                return EditorGUIUtility.FindTexture("PrefabNormal Icon");
            case ".shader":
                return EditorGUIUtility.FindTexture("Shader Icon");
            case ".txt":
                return EditorGUIUtility.FindTexture("TextAsset Icon");
            case ".unity":
                return EditorGUIUtility.FindTexture("SceneAsset Icon");
            case ".asset":
            case ".prefs":
                return EditorGUIUtility.FindTexture("GameManager Icon");
            case ".anim":
                return EditorGUIUtility.FindTexture("Animation Icon");
            case ".meta":
                return EditorGUIUtility.FindTexture("MetaFile Icon");
            case ".ttf":
            case ".otf":
            case ".fon":
            case ".fnt":
                return EditorGUIUtility.FindTexture("Font Icon");
            case ".aac":
            case ".aif":
            case ".aiff":
            case ".au":
            case ".mid":
            case ".midi":
            case ".mp3":
            case ".mpa":
            case ".ra":
            case ".ram":
            case ".wma":
            case ".wav":
            case ".wave":
            case ".ogg":
                return EditorGUIUtility.FindTexture("AudioClip Icon");
            case ".ai":
            case ".apng":
            case ".png":
            case ".bmp":
            case ".cdr":
            case ".dib":
            case ".eps":
            case ".exif":
            case ".gif":
            case ".ico":
            case ".icon":
            case ".j":
            case ".j2c":
            case ".j2k":
            case ".jas":
            case ".jiff":
            case ".jng":
            case ".jp2":
            case ".jpc":
            case ".jpe":
            case ".jpeg":
            case ".jpf":
            case ".jpg":
            case ".jpw":
            case ".jpx":
            case ".jtf":
            case ".mac":
            case ".omf":
            case ".qif":
            case ".qti":
            case ".qtif":
            case ".tex":
            case ".tfw":
            case ".tga":
            case ".tif":
            case ".tiff":
            case ".wmf":
            case ".psd":
            case ".exr":
                return EditorGUIUtility.FindTexture("Texture Icon");
            case ".3df":
            case ".3dm":
            case ".3dmf":
            case ".3ds":
            case ".3dv":
            case ".3dx":
            case ".blend":
            case ".c4d":
            case ".lwo":
            case ".lws":
            case ".ma":
            case ".max":
            case ".mb":
            case ".mesh":
            case ".obj":
            case ".vrl":
            case ".wrl":
            case ".wrz":
            case ".fbx":
                return EditorGUIUtility.FindTexture("Mesh Icon");
            case ".asf":
            case ".asx":
            case ".avi":
            case ".dat":
            case ".divx":
            case ".dvx":
            case ".mlv":
            case ".m2l":
            case ".m2t":
            case ".m2ts":
            case ".m2v":
            case ".m4e":
            case ".m4v":
            case ".mjp":
            case ".mov":
            case ".movie":
            case ".mp21":
            case ".mp4":
            case ".mpe":
            case ".mpeg":
            case ".mpg":
            case ".mpv2":
            case ".ogm":
            case ".qt":
            case ".rm":
            case ".rmvb":
            case ".wmw":
            case ".xvid":
                return EditorGUIUtility.FindTexture("MovieTexture Icon");
            case ".colors":
            case ".gradients":
            case ".curves":
            case ".curvesnormalized":
            case ".particlecurves":
            case ".particlecurvessigned":
            case ".particledoublecurves":
            case ".particledoublecurvessigned":
                return EditorGUIUtility.FindTexture("ScriptableObject Icon");
        }
        return EditorGUIUtility.FindTexture("DefaultAsset Icon");
    }
}
