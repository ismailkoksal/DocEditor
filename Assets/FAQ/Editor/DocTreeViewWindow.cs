﻿using UnityEngine;
using UnityEditor.IMGUI.Controls;
using UnityEditor;
using System;

public class DocTreeViewWindow : EditorWindow
{
    [SerializeField]
    private TreeViewState treeViewState;

    private DocTreeView treeView;
    private SearchField m_SearchField;

    private EditorGUISplitView horizontalSplitView = new EditorGUISplitView(EditorGUISplitView.Direction.Horizontal);

    private void OnEnable()
    {
        if (treeViewState == null)
            treeViewState = new TreeViewState();

        treeView = new DocTreeView(treeViewState);
        m_SearchField = new SearchField();
        m_SearchField.downOrUpArrowKeyPressed += treeView.SetFocusAndEnsureSelectedItem;
    }

    private void OnGUI()
    {
        horizontalSplitView.BeginSplitView();
        DoToolbar();
        DoTreeView();
        horizontalSplitView.Split();
        horizontalSplitView.EndSplitView();
        Repaint();
    }

    private void OnInspectorUpdate()
    {
        EditorGUI.BeginChangeCheck();
        if (EditorGUI.EndChangeCheck())
        {
            treeView.Reload();
        }
    }

    private void Reload()
    {
        treeView.Reload();
    }

    private void DoToolbar()
    {
        GUILayout.BeginHorizontal(EditorStyles.toolbar);
        if (GUILayout.Button("Settings", EditorStyles.toolbarButton))
        {
            string path = EditorUtility.OpenFolderPanel("Set Path", "", "");
            if (path.Length != 0)
            {
                try
                {
                    EditorPrefs.SetString(treeView.PathKey, path);
                    Reload();
                } catch (UnauthorizedAccessException) { }
            }
        }
        GUILayout.Space(100);
        GUILayout.FlexibleSpace();
        try
        {
            treeView.searchString = m_SearchField.OnToolbarGUI(treeView.searchString);
        }
        catch { }
        GUILayout.EndHorizontal();
    }

    private void DoTreeView()
    {
        Rect rect = GUILayoutUtility.GetRect(0, 100000, 0, 100000);
        treeView.OnGUI(rect);
    }

    [MenuItem("Cottos/Documentation")]
    public static void ShowWindow()
    {
        var window = GetWindow<DocTreeViewWindow>();
        window.titleContent = new GUIContent("Documentation");
        window.Show();
    }
}